

修订记录

 

| 日期 | 修订版本 | 修改章节 | 修改描述 |
| ---- | -------- | -------- | -------- |
|      |          |          |          |

缩略语清单： 

| 缩略语 | 英文全名 | 中文解释 |
| ------ | -------- | -------- |
|        |          |          |
|        |          |          |
|        |          |          |
|        |          |          |

# 

# 1   概述

描述本次被测对象变更内容。

# 2   测试版本说明

描述测试版本信息。

| 版本名称            | 测试起始时间 | 测试结束时间 |
| ------------------- | ------------ | ------------ |
| OpenHarmony 3.0.0.6 | 2021/8/25    | 2021/9/7     |

描述本次测试的测试环境（包括环境软硬件版本信息，环境组网配置信息, 测试辅助工具等）。

| 硬件型号        | 硬件配置信息 | 备注                                              |
| --------------- | ------------ | ------------------------------------------------- |
| hispark_pegasus | Hi3861开发板 | docs/zh-cn/device-dev/quick-start/Hi3861开发板.md |
| hispark_aries   | Hi3518开发板 | docs/zh-cn/device-dev/quick-start/Hi3518开发板.md |
| hispark_taurus  | Hi3516开发板 | docs/zh-cn/device-dev/quick-start/Hi3516开发板.md |

 

# 3   版本概要测试结论

*本章节概要给出版本测试结论*

# 4   版本详细测试结论

*本章节针对总体测试策略计划的测试内容，给出详细的测试结论。*

## 4.1   特性测试结论



### 4.1.1   继承特性评价

*继承特性进行评价，用表格形式评价，包括特性列表，验证质量评估。*

XXX子系统：特性质量良好

| 序号 | 特性名称   | 特性质量评估               | 备注 |
| ---- | ---------- | -------------------------- | ---- |
| 1    | 内核子系统 | 没有新增特性，基本功能可用 |      |
| 2   | DFX子系统 | 新增faultlogger功能，覆盖对应的功能、稳定性压测，性能和安全不涉及，功能正常，无异常，其余继承功能覆盖自动化和稳定性压测，hilog压测之后hilogd进程异常重启，且hilog命令无法执行 |  https://gitee.com/openharmony/hiviewdfx_hilog/issues/I48IM7?from=project-issue    |
|  3  | 全球化子系统 | 无新增特性，测试用例执行通过，特性质量良好 | |
|  4  | 驱动子系统 | 新增特性，测试用例执行通过。camera、wifi、display特性不稳定 | |
|  5  | 多媒体子系统 | 无新增特性，测试用例执行通过，特性质量良好 | |
|  6  | 启动恢复子系统 | 无新增特性，测试用例执行通过，特性质量良好 | |
|  7  | 数据管理子系统 | 无新增特性，测试用例执行通过，特性质量良好 | |
|  8  | 集成测试 | 新需求基础功功能可用，遗留少量问题，继承性功能分布式设备无法发现，特性不稳定，风险较高 |特性不稳定，风险高 |https://gitee.com/openharmony/app_samples/issues/I48RU8  分布式数据管理模块在跟踪|
|  9  | 电源子系统 | 无新增特性，测试用例执行通过，特性质量良好 | |
*特性质量评估可选项：特性不稳定，风险高\特性基本可用，遗留少量问题\特性质量良好*

### 4.1.2   新需求评价

*以表格的形式汇总新特性测试执行情况及遗留问题情况的评估,给出特性质量评估结论。*

| lssue号                                                      | 特性名称                                                     | 特性质量评估             | 约束依赖说明 | 备注     |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------ | ------------ | -------- |
| [I3NN1Z](https://gitee.com/openharmony/aafwk_aafwk_lite/issues/I3NN1Z) | 【应用程序框架】轻量级实现弹窗授权动态授权机制               | 需求未交付，版本计划错误 |              | 小型系统 |
| [I40PBC](https://gitee.com/open_harmony/dashboard?issue_id=I40PBC) | 应用侧发布本地分组的普通通知                                 | 需求未交付，版本计划错误 |              | 标准系统 |
| [I40PBF](https://gitee.com/open_harmony/dashboard?issue_id=I40PBF) | 在免打扰模式下发布通知                                       | 需求未交付，版本计划错误 |              | 标准系统 |
| [I40PBG](https://gitee.com/open_harmony/dashboard?issue_id=I40PBG) | 发布开启一个无页面的Ability的wantAgent                       | 需求未交付，版本计划错误 |              | 标准系统 |
| [I40PBH](https://gitee.com/open_harmony/dashboard?issue_id=I40PBH) | 取消WantAgent的实例                                          | 需求未交付，版本计划错误 |              | 标准系统 |
| [I40PBI](https://gitee.com/open_harmony/dashboard?issue_id=I40PBI) | 发布公共事件的WantAgent通知                                  | 需求未交付，版本计划错误 |              | 标准系统 |
| [I40PBM](https://gitee.com/open_harmony/dashboard?issue_id=I40PBM) | 应用侧取消本地通知                                           | 需求未交付，版本计划错误 |              | 标准系统 |
| [I40PBN](https://gitee.com/open_harmony/dashboard?issue_id=I40PBN) | 应用侧发布声音通知                                           | 需求未交付，版本计划错误 |              | 标准系统 |
| [I40PBO](https://gitee.com/open_harmony/dashboard?issue_id=I40PBO) | 应用侧发布振动通知                                           | 需求未交付，版本计划错误 |              | 标准系统 |
| [I40PBP](https://gitee.com/open_harmony/dashboard?issue_id=I40PBP) | 应用侧发布本地有输入框的通知（NotificationUserInput）        | 需求未交付，版本计划错误 |              | 标准系统 |
| I41H55                                                       | 【驱动子系统】基于HDF驱动框架提供I2S/PCM平台总线驱动         | 特性基本可用             | 不涉及       |          |
| I41HBF                                                       | 【驱动子系统】基于HDF驱动框架提供Display驱动模型兼容DRM显示框架 | 特性基本可用             | 不涉及       |          |
|  I3ZVTJ                                                       |   【分布式任务调度子系统】接收远端拉起FA请求，跨设备拉起远端FA  |   特性基本可用      |  不涉及            |          |
|  I3ZVTT                                                      |   【分布式任务调度子系统】鸿蒙单框架L2分布式能力建设-SAMGR模块构建 |   特性基本可用      |  不涉及            |          |
| I45ZKP | RQ-[Demo&应用子系统][JSUI][画布组件] JsCanvas| 特性基本可用，遗留少量问题| 不涉及|https://gitee.com/openharmony/app_samples/issues/I48HJN  &  https://gitee.com/openharmony/app_samples/issues/I48HLN|
| I466KX| RQ-[Demo&应用子系统][JSUI]【OpenHarmony】 栅格布局| 特性质量良好| 不涉及| |
| I45YM2| RQ-[Demo&应用子系统][JS UI]【OpenHarmony】JS自定义组件（JSUICustomComponent）| 特性质量良好| 不涉及| |
| I44Y4J|RQ-[Demo&应用子系统][JS UI]【HarmonyOS】自适应卡片（AdaptiveServiceWidget）| 特性质量良好| 不涉及| |
| I44Y0N| RQ-[Demo&应用子系统][JS UI]【HarmonyOS】自适应页面（AdaptivePortalPage）| 特性质量良好| 不涉及| |
| I44Y15| RQ-[Demo&应用子系统][JS UI]【HarmonyOS】自适应效率型首页（AdaptivePortalList）| 特性质量良好| 不涉及| |
| I46ZCN| RQ-[Demo&应用子系统][JAVA UI]【HarmonyOS】添加NativeLayer示例| 特性质量良好| 不涉及| |
*特性质量评估可选项：特性不稳定，风险高\特性基本可用，遗留少量问题\特性质量良好*

## 4.2   兼容性测试结论

*补充兼容性测试报告*

## 4.3   安全专项测试结论

*要求：无严重的隐私安全问题遗留，版本通过漏洞扫描，未解决的漏洞满足以下要求：*

*a）无严重及以上公开漏洞；*

*b）无已公开60天但未修复的一般（CVSS得分4及以上）安全漏洞；*

*c）不存在已知CVE安全漏洞，业界未解决的已知CVE安全漏洞可例外；*

*d）无法修复，且经过社区开发团队综合评估备案允许遗留的漏洞；*

## 4.4   稳定性专项测试结论

| 测试分类 | 测试评估项 | 质量目标 | 是否满足 | 测试结果及关键遗留问题/风险 | 备注issue |
| -------- | ---------- | -------- | -------- | --------------------------- | --------- |
|  开关机测试        |  整机稳定性          | 无不开机、无整机稳定性问题         | 否         |  测试结果：多个设备重启之后导致hdc无法连接设备                           | https://gitee.com/openharmony/developtools_hdc_standard/issues/I47XAE?from=project-issue          |
|  xts压测        |  各子系统在整机测试下的稳定性          |  无稳定性问题        |  否        |  测试结果：无  <br/>风险：新环境还未完成环境部署，xts用例还未执行起来</br>                           |           |

## 4.5   性能专项测试结论

*以表格形式汇总各专项测试执行情况及遗留问题情况的评估，给出专项质量评估结论*

*要求：1、静态KPI通过率100%，2、开关机及动态内存整机达标，子系统无严重问题

| 测试分类 | 测试评估项 | 质量目标 | 是否满足 | 测试结果及关键遗留问题/风险 | 备注issue |
| -------- | ---------- | -------- | -------- | --------------------------- | --------- |
| 基础性能 | 轻量系统静态模型 | 静态KPI通过率100% | 满足   |  轻量系统无感配网满足版本质量 |           |
| 基础性能 | 小型系统静态模型 | 静态KPI通过率100% | 不满足 |  小型系统图库、查看图片、拍照、录像均满足测试要求|   |
| 基础性能 | 标准系统静态模型 | 静态KPI通过率100% | 无法评估 |  标准系统在当前版本间无劣化正常 | 当前标准系统无静态基线 |
| 基础性能 | 轻量系统内存专项 | 开机以及动态内存整机达标，子系统无严重问题 | 满足   |  轻量系统无感配网满足版本质量 |           |
| 基础性能 | 小型系统内存专项 | 开机以及动态内存整机达标，子系统无严重问题 | 不满足 |  3516以及3518常驻内存均未达标 | https://gitee.com/openharmony/community/issues/I434AD  https://gitee.com/openharmony/community/issues/I434P1|
| 基础性能 | 标准系统静态模型 | 开机以及动态内存整机达标，子系统无严重问题 | 无法评估 |   开机以及动态内存整机达标，子系统无严重问题 | 当前标准系统无内存基线 |

## 4.6   功耗专项测试结论

*以表格形式汇总各专项测试执行情况及遗留问题情况的评估，给出专项质量评估结论*

*要求：无严重问题

| 测试分类 | 测试评估项 | 质量目标 | 是否满足 | 测试结果及关键遗留问题/风险 | 备注issue |
| -------- | ---------- | -------- | -------- | --------------------------- | --------- |
| 待机/场景功耗 | 小型系统待机/场景功耗 | 待机功耗达标 |  满足  | 待机/场景质量满足版本质量要 |           |
| 待机/场景功耗 | 标准系统待机/场景功耗 | 待机功耗达标 |  无法评估  | 标准系统无待机/场景功耗基线  |           |

# 5   问题单统计
https://gitee.com/openharmony/drivers_peripheral/issues/I43339 【OpenHarmony 2.2 Beta2】【驱动子系统】L2单板camera DFx测试用例失败
https://gitee.com/openharmony/drivers_peripheral/issues/I48A2I 【OpenHarmony 3.0.0.6】【驱动子系统】L1 3516DV300单板liteos版本调用AllocMem接口测试，单板挂死
https://gitee.com/openharmony/drivers_peripheral/issues/I47DE6 【OpenHarmony 3.0 Beta1】【驱动子系统】不安全函数和安全编译选项
https://gitee.com/openharmony/drivers_peripheral/issues/I46HH7 【OpenHarmony 3.0.0.3】【驱动子系统】L2单板wifi测试用例失败
https://gitee.com/openharmony/multimedia_camera_lite/issues/I434AD  HI3516DV300 L1常驻内存超基线
https://gitee.com/openharmony/multimedia_camera_lite/issues/I434P1  HI3518EV300常驻内存超基线
